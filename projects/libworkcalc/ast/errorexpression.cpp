#include "errorexpression.h"

bool ErrorExpressionAST::isInvalid() const
{
    return true;
}

std::string ErrorExpressionAST::toString() const
{
    return utils::format("error \'%s\'", m_error.c_str());
}

std::vector<const std::unique_ptr<ExpressionAST> *> ErrorExpressionAST::childs() const
{
    std::vector<const std::unique_ptr<ExpressionAST> *> vec;
    vec.push_back(&m_child);
    return vec;
}

llvm::Value *ErrorExpressionAST::codegen(VMContext* ctx)
{
    return nullptr;
}
