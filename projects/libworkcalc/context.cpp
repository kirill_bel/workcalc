#include "context.h"

#include "codegen/vmcontext_p.h"

WorkCalcContext::WorkCalcContext() :
    m_context(new VMContext())
{

}

WorkCalcContext::~WorkCalcContext()
{

}

VMContext *WorkCalcContext::context()
{
    return m_context.get();
}
