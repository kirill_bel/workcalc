#ifndef NUMBEREXPRESSION_H
#define NUMBEREXPRESSION_H

#include "ast/ast.h"

class NumberExpressionAST : public ExpressionAST
{
    union Value
    {
        Value(int64_t value) : valueInt(value) {}
        Value(double value) : valueFloat(value) {}

        int64_t valueInt;
        double valueFloat;
    } m_value;

    ValueType m_type;

public:
    NumberExpressionAST(size_t beginIndex, size_t size, int64_t value) :
        ExpressionAST(beginIndex, size),
        m_value(value), m_type(kInteger) {}
    NumberExpressionAST(size_t beginIndex, size_t size, double value) :
        ExpressionAST(beginIndex, size),
        m_value(value), m_type(kFloat) {}

    bool isInvalid() const override;
    std::string toString() const override;
    llvm::Value* codegen(VMContext* ctx) override;
};

#endif // NUMBEREXPRESSION_H
