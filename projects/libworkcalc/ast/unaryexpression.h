#ifndef UNARYEXPRESSION_H
#define UNARYEXPRESSION_H

#include "ast/ast.h"

class UnaryExpressionAST : public ExpressionAST
{
    std::string m_operation;
    std::unique_ptr<ExpressionAST> m_rhs;
public:
    UnaryExpressionAST(size_t beginIndex, size_t size,
                        const std::string& operation,
                        std::unique_ptr<ExpressionAST> RHS) :
        ExpressionAST(beginIndex, size),
        m_operation(operation),
         m_rhs(std::move(RHS)) {}

    bool isInvalid() const override;
    std::string toString() const override;
    std::vector< const std::unique_ptr<ExpressionAST>* > childs() const override;
    llvm::Value* codegen(VMContext* ctx) override;
};

#endif // UNARYEXPRESSION_H
