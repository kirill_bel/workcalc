#include "callexpression.h"

#include "codegen/vmcontext_p.h"

bool CallExpressionAST::isInvalid() const
{
    return m_callee.empty();
}

std::string CallExpressionAST::toString() const
{
    return utils::format("call \'%s\'", m_callee.c_str());
}

std::vector<const std::unique_ptr<ExpressionAST> *> CallExpressionAST::childs() const
{
    std::vector<const std::unique_ptr<ExpressionAST> *> vec;
    for(int i = 0; i < m_arguments.size(); i++)
    {
        vec.push_back(&(m_arguments[i]));
    }
    return vec;
}

llvm::Value *CallExpressionAST::codegen(VMContext* ctx)
{
    llvm::Function* calee_func = ctx->module->getFunction(m_callee);
    llvm::Value* calee_ptr = nullptr;
    llvm::FunctionType* fType = nullptr;

    if(calee_func)
        fType = calee_func->getFunctionType();
    else if(calee_ptr = ctx->module->getGlobalVariable(m_callee))
    {
        // Trying to deduce function type from pointer
        if(!calee_ptr->getType()->isPointerTy())
        {
            utils::error("This is not a pointer!");
            return nullptr;
        }

        llvm::PointerType* pTy = static_cast<llvm::PointerType*>(calee_ptr->getType());
        if(!pTy || !pTy->getElementType() ||
                !pTy->getElementType()->getContainedType(0) ||
                !pTy->getElementType()->getContainedType(0)->isFunctionTy())
        {
            utils::error("Something went wrong!");
            return nullptr;
        }

        fType = static_cast<llvm::FunctionType*>(pTy->getElementType()->getContainedType(0));
    }
    else
    {
        utils::error("Unknown function name: \"%s\"", m_callee.c_str());
        return nullptr;
    }

    if(!fType)
    {
        utils::error("Can't find a function type");
        return nullptr;
    }


    if(fType->getNumParams() != m_arguments.size())
    {
        utils::error("Invalid argument count in function \"%s\": %d (must be %d)",
                     m_callee.c_str(),
                     m_arguments.size(),
                     fType->getNumParams());
        return nullptr;
    }

    std::vector<llvm::Value*> arg_values;
    for ( auto &arg : m_arguments )
    {
        arg_values.push_back(arg->codegen(ctx));
        if(arg_values.back() == 0)
        {
            return nullptr;
        }
    }

    if(calee_func)
        return ctx->builder->CreateCall(calee_func, arg_values, "calltmp");
    return ctx->builder->CreateCall(fType, calee_ptr, arg_values, "calltmp");;
}
