#include "lambdaexpression.h"

#include <llvm/IR/Verifier.h>
#include "codegen/vmcontext_p.h"

bool LambdaExpressionAST::isInvalid() const
{
    return false;//m_body->isInvalid();
}

std::string LambdaExpressionAST::toString() const
{
    std::string str;
    std::string arguments;
    for(int i = 0; i < m_arguments.size(); i++)
    {
        arguments += m_arguments[i];
        if(i < m_arguments.size() - 1)
            arguments += ", ";
    }

    return utils::format("lambda \'%s\'", arguments.c_str());
}

std::vector<const std::unique_ptr<ExpressionAST> *> LambdaExpressionAST::childs() const
{
    std::vector<const std::unique_ptr<ExpressionAST> *> vec;
    vec.push_back(&m_body);
    return vec;
}

llvm::Value *LambdaExpressionAST::codegen(VMContext* ctx)
{
    std::vector<llvm::Type*> types(m_arguments.size(),
                                   llvm::Type::getDoubleTy(ctx->llvm_context()));

    llvm::FunctionType* func_type = llvm::FunctionType::get(
                llvm::Type::getDoubleTy(ctx->llvm_context()),
                types,
                false);

    llvm::Function* func = llvm::Function::Create(func_type,
                                                  llvm::Function::ExternalLinkage,
                                                  m_name.empty() ? "___anon_expr" :
                                                                   m_name.c_str(),
                                                  ctx->module.get());

//    llvm::Function::arg_iterator iterator = func->arg_begin();
//    for ( auto &arg : m_arguments )
//    {
//        Value *val = iterator++;
//        val->setName(arg);
//    }

    llvm::BasicBlock *block = llvm::BasicBlock::Create(ctx->llvm_context(),
                                                       "entry",
                                                       func);
    auto oldBlock = ctx->builder->GetInsertBlock();
    ctx->builder->SetInsertPoint(block);   

    ctx->clearNamedValues();
//    for (auto &arg : func->args())
//        ctx->setNamedValue(arg.getName(), &arg);


    // Create argument allocations
    llvm::Function::arg_iterator iterator = func->arg_begin();
    for ( auto &arg : m_arguments)
    {
        llvm::Value *val = iterator++;
        llvm::AllocaInst *alloca = ctx->createEntryBlockAlloca(func, arg, val->getType());
        ctx->builder->CreateStore(val, alloca);
        ctx->setNamedValue(arg, alloca);
    }



    if(m_body)
    {
        if(llvm::Value* retval = m_body->codegen(ctx))
        {
            // Finish off the function.
            ctx->builder->CreateRet(retval);
            // Validate the generated code, checking for consistency.
            llvm::verifyFunction(*func);
        }
    }

    if(oldBlock)
        ctx->builder->SetInsertPoint(oldBlock);

//    if(llvm::Value* retval = m_body->codegen(ctx))
//    {
//        // Finish off the function.
//        ctx->builder->CreateRet(retval);
//        // Validate the generated code, checking for consistency.
//        llvm::verifyFunction(*func);
//        return func;
//    }
    return func;
}
