#include "binaryexpression.h"

#include "codegen/vmcontext_p.h"
#include "ast/variableexpression.h"

bool BinaryExpressionAST::isInvalid() const
{
    return m_operation.empty() ||
            (m_lhs && m_lhs->isInvalid()) ||
            (m_rhs && m_rhs->isInvalid());
}

std::string BinaryExpressionAST::toString() const
{
    return utils::format("binop \'%s\'", m_operation.c_str());
}

std::vector<const std::unique_ptr<ExpressionAST> *> BinaryExpressionAST::childs() const
{
    std::vector<const std::unique_ptr<ExpressionAST> *> vec;
    vec.push_back(&m_lhs);
    vec.push_back(&m_rhs);
    return vec;
}

llvm::Value *BinaryExpressionAST::codegen(VMContext* ctx)
{
    if(m_operation == "=")
    {
        VariableExpressionAST* lhs_expr =
                dynamic_cast<VariableExpressionAST*>(m_lhs.get());
        if(!lhs_expr)
        {
            utils::error("Left side of equality operator must be a variable");
            return nullptr;
        }

        llvm::Value* val = m_rhs->codegen(ctx);
        if(!val)
        {
            return nullptr;
        }

        llvm::Value* variable = ctx->namedValue(lhs_expr->name());
        if(!variable)
        {
//            variable = ctx->builder->CreateAlloca(val->getType(), 0,
//                                               lhs_expr->name().c_str());
            variable = ctx->createGlobalVariable(lhs_expr->name(), val->getType());
            if(variable)
            {
                ctx->setNamedValue(lhs_expr->name(), static_cast<llvm::AllocaInst*>(variable));
                utils::error("Variable created: \"%s\", type %d",
                             lhs_expr->name().c_str(),
                             val->getType()->getTypeID());
            }
            else
            {
                utils::error("Failed to create variable: \"%s\"", lhs_expr->name().c_str());
                return nullptr;
            }
        }

        ctx->builder->CreateStore(val, variable);
        return val;
    }

    llvm::Value *lhs_val = m_lhs->codegen(ctx);
    llvm::Value *rhs_val = m_rhs->codegen(ctx);
    if (!lhs_val || !rhs_val)
        return nullptr;

    ValueType ft = kNone;
    ValueType valType = prepareValues(ctx, lhs_val, rhs_val, ft);
    if(valType == kNone)
    {
        utils::error("Failed to prepare values");
        return nullptr;
    }

    bool isFloat = lhs_val->getType()->isDoubleTy();

    switch (valType)
    {
    case kFloat:
        switch(m_operation[0])
        {
        case '+': return ctx->builder->CreateFAdd(lhs_val, rhs_val, "addtmp");
        case '-': return ctx->builder->CreateFSub(lhs_val, rhs_val, "subtmp");
        case '*': return ctx->builder->CreateFMul(lhs_val, rhs_val, "multmp");
        case '/': return ctx->builder->CreateFDiv(lhs_val, rhs_val, "divtmp");
        }
        break;
    case kInteger:
        switch(m_operation[0])
        {
        case '+': return ctx->builder->CreateAdd(lhs_val, rhs_val, "addtmp");
        case '-': return ctx->builder->CreateSub(lhs_val, rhs_val, "subtmp");
        case '*': return ctx->builder->CreateMul(lhs_val, rhs_val, "multmp");
        case '/': return ctx->builder->CreateSDiv(lhs_val, rhs_val, "divtmp");
        }
        break;
    default:
        utils::error("Unknown value type");
        return nullptr;
    }

    utils::error("Unknown binary operator: %s", m_operation.c_str());
    return nullptr;
}

ExpressionAST::ValueType BinaryExpressionAST::prepareValues(VMContext* ctx, llvm::Value *&v1, llvm::Value *&v2, ValueType ft)
{
    if(v1->getType()->isPointerTy())
        v1 = ctx->builder->CreateLoad(v1, "dataptr");
    if(v2->getType()->isPointerTy())
        v2 = ctx->builder->CreateLoad(v2, "dataptr");

    ValueType valType = kNone;
    if(ft == kNone)
    {
        if(v1->getType()->getTypeID() != v2->getType()->getTypeID())
        {
            valType = kFloat;
            if(v1->getType()->isDoubleTy() && v2->getType()->isIntegerTy())
            {
                v2 = ctx->builder->CreateSIToFP(v2, llvm::Type::getDoubleTy(ctx->llvm_context()), "floattmp");
            }
            else if(v1->getType()->isIntegerTy() && v2->getType()->isDoubleTy())
            {
                v1 = ctx->builder->CreateSIToFP(v1, llvm::Type::getDoubleTy(ctx->llvm_context()), "floattmp");
            }
            else
            {
                utils::error("Typecast failed");
                return kNone;
            }
        }
        else
        {
            switch(v1->getType()->getTypeID())
            {
            case llvm::Type::IntegerTyID:
                valType = kInteger;
                break;
            case llvm::Type::DoubleTyID:
                valType = kFloat;
                break;
            default:
                utils::error("Unknown value type");
            }
        }
    }
    return valType;
}
