#ifndef VMMODULE_H
#define VMMODULE_H

#include <memory>

#include <llvm/ExecutionEngine/ExecutionEngine.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>

class VmVariable;
class VmFunction;
class VmModule
{
public:
    VmModule(std::unique_ptr<llvm::Module> module);

    std::shared_ptr<VmVariable> getFloatVariable(std::string name, bool bInsert = true);
    std::shared_ptr<VmVariable> getDoubleVariable(std::string name, bool bInsert = true);
    std::shared_ptr<VmVariable> getFunctionPtrVariable(std::string name, bool bInsert = true);

    std::shared_ptr<VmFunction> addFunction(llvm::Function* func);
private:
    std::unique_ptr<llvm::Module> m_module;
};

#endif // VMMODULE_H
