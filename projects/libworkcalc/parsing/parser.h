#ifndef PARSER_H
#define PARSER_H

#include <cstdint>
#include <string>
#include <memory>
#include <vector>
#include <map>

#include "parsing/lexer.h"
#include "ast/ast.h"
#include "common.h"

class Parser
{
    std::map<std::string, int> m_operatorPrecedence;

    enum NumberFormat
    {
        kBinaryInteger,
        kHexInteger,
        kDecimalInteger
    };

    static std::vector<std::string> DebugPrintReq(const std::unique_ptr<ExpressionAST>& expr, int level = 0);
public:
    Parser();

    static std::string DebugPrint(const std::unique_ptr<ExpressionAST>& expr);

    int parse(const std::vector<Lexer::Token>& tokens,
              std::vector< std::unique_ptr<ExpressionAST> >* outAST);

protected:
    bool isEnd(const Lexer::Token* token) const;
//    bool isInvalid(const std::unique_ptr<ExpressionAST>& expr) const;
    bool testToken(int offset, Lexer::TokenType type, std::string text = "", bool caseSensitive = true) const;
    bool isBinaryOp(int offset);
    bool isUnaryOp(int offset);

    std::unique_ptr<ExpressionAST> parseBody();
    std::unique_ptr<ExpressionAST> parsePrimary();
    std::unique_ptr<ExpressionAST> parseNumber();
    std::unique_ptr<ExpressionAST> parseBrackets();
    std::unique_ptr<ExpressionAST> parseIdent();
    std::unique_ptr<ExpressionAST> parseBinOpRHS(int expressionPrecedence, std::unique_ptr<ExpressionAST> LHS);
    std::unique_ptr<ExpressionAST> parseUnOp();
    std::unique_ptr<ExpressionAST> parseLambda();
    std::unique_ptr<ExpressionAST> parseExpression();
    std::unique_ptr<ExpressionAST> parseTopLevelExpression();
    std::unique_ptr<ExpressionAST> errorExpr(std::unique_ptr<ExpressionAST> expr, std::string str);
    std::unique_ptr<ExpressionAST> error(const Lexer::Token* token, std::string str);
    std::unique_ptr<ExpressionAST> errorExprFmt(std::unique_ptr<ExpressionAST> expr, const char* fmt, ...)
    {
        va_list args;
        va_start(args, fmt);
        std::string str = utils::format_va(fmt, args);
        va_end(args);
        return errorExpr(std::move(expr), str);
    }
    std::unique_ptr<ExpressionAST> errorFmt(const Lexer::Token* token, const char* fmt, ...)
    {
        va_list args;
        va_start(args, fmt);
        std::string str = utils::format_va(fmt, args);
        va_end(args);
        return error(token, str);
    }


    int64_t toInteger(const std::string& str, int base, std::string* whatError);
    double toFloat(const std::string& str, std::string* whatError);
    int getCurrentTokenPrecedence() const;

private:
    const Lexer::Token* m_begin = nullptr;
    const Lexer::Token* m_cur = nullptr;
    const Lexer::Token* m_end = nullptr;
};

#endif // PARSER_H
