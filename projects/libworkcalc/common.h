#ifndef COMMON_H
#define COMMON_H

#include <cstdio>
#include <cstdarg>
#include <string>
#include <iostream>

namespace utils
{
    std::string format_va(const char* fmt, va_list args);

    inline std::string format(const char* fmt, ...)
    {
        va_list args;
        va_start(args, fmt);

        std::string str = format_va(fmt, args);

        va_end(args);
        return str;
    }

    inline void error(const char* fmt, ...)
    {
        va_list args;
        va_start(args, fmt);

        std::string str = format_va(fmt, args);

        va_end(args);
        std::cout << "ERROR: " << str << std::endl;
    }
}

#endif // COMMON_H
