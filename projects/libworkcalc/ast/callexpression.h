#ifndef CALLEXPRESSION_H
#define CALLEXPRESSION_H

#include "ast/ast.h"

class CallExpressionAST : public ExpressionAST
{
    std::string m_callee;
    std::vector< std::unique_ptr<ExpressionAST> > m_arguments;
public:
    CallExpressionAST(size_t beginIndex, size_t size,
                      const std::string& callee,
                      std::vector< std::unique_ptr<ExpressionAST> > arguments) :
        ExpressionAST(beginIndex, size),
        m_callee(callee),
        m_arguments(std::move(arguments)) {}

    bool isInvalid() const override;
    std::string toString() const override;
    std::vector< const std::unique_ptr<ExpressionAST>* > childs() const override;
    llvm::Value* codegen(VMContext* ctx) override;
};


#endif // CALLEXPRESSION_H
