#include "parser.h"

#include <algorithm>
#include <cctype>

#include "ast/binaryexpression.h"
#include "ast/callexpression.h"
#include "ast/errorexpression.h"
#include "ast/lambdaexpression.h"
#include "ast/numberexpression.h"
#include "ast/unaryexpression.h"
#include "ast/variableexpression.h"

Parser::Parser()
{   
    m_operatorPrecedence["<-"] = 99;
    m_operatorPrecedence["->"] = 99;

    m_operatorPrecedence["="] = 100;

    m_operatorPrecedence["||"] = 200;

    m_operatorPrecedence["&&"] = 300;

    m_operatorPrecedence["|"] = 400;

    m_operatorPrecedence["&"] = 500;

    m_operatorPrecedence["<"] = 600;
    m_operatorPrecedence[">"] = 600;

    m_operatorPrecedence["<<"] = 700;
    m_operatorPrecedence[">>"] = 700;

    m_operatorPrecedence["+"] = 800;
    m_operatorPrecedence["-"] = 800;

    m_operatorPrecedence["*"] = 900;
    m_operatorPrecedence["/"] = 900;

    m_operatorPrecedence["^"] = 1000;

    m_operatorPrecedence["!"] = 1100;
    m_operatorPrecedence["~"] = 1100;
}

int Parser::parse(const std::vector<Lexer::Token> &tokens,
                  std::vector< std::unique_ptr<ExpressionAST> >* outAST)
{
    m_begin = &(tokens[0]);
    m_cur = m_begin;
    m_end = m_begin + tokens.size();

    std::vector< std::unique_ptr<ExpressionAST> >& expressions = *outAST;

    auto ex = std::make_unique<LambdaExpressionAST>(0, m_end - m_begin,
                                              std::vector<std::string>(),
                                              nullptr, "main");
    expressions.push_back(std::move(ex));

    while(!isEnd(m_cur))
    {
        switch (m_cur->type)
        {
        case Lexer::kEOF:
            return 0;
        case Lexer::kSemicolon:
            m_cur++;
            break;
        default:
            expressions.push_back(parseTopLevelExpression());
        }
    }



    return 0;
}

bool Parser::isEnd(const Lexer::Token *token) const
{
    return token >= m_end;
}

bool Parser::testToken(int offset,
                       Lexer::TokenType type,
                       std::string text,
                       bool caseSensitive) const
{
    if(isEnd(m_cur + offset))
        return false;

    const Lexer::Token* token = m_cur + offset;
    if(token->type != type)
        return false;

    if(!text.empty())
    {
        if(caseSensitive)
        {
            if(token->text != text)
                return false;
        }
        else
        {
            std::string s1 = token->text;
            std::string s2 = text;
            std::transform(s1.begin(), s1.end(), s1.begin(), std::tolower);
            std::transform(s2.begin(), s2.end(), s2.begin(), std::tolower);

            if(s1 != s2)
                return false;
        }
    }

    return true;
}

bool Parser::isBinaryOp(int offset)
{
    if(!testToken(offset, Lexer::kOperator))
        return false;

    const Lexer::Token* op = m_cur + offset;
    switch(op->text[0])
    {
    case '+': return true;
    case '-': return true;
    }

    return !isUnaryOp(offset);
}

bool Parser::isUnaryOp(int offset)
{
    if(!testToken(offset, Lexer::kOperator))
        return false;

    const Lexer::Token* op = m_cur + offset;
    switch(op->text[0])
    {
    case '+': return true;
    case '-': return true;
    case '~': return true;
    case '!': return true;
    }
    return false;
}

std::unique_ptr<ExpressionAST> Parser::parsePrimary()
{
    switch(m_cur->type)
    {
    case Lexer::kIdent:
        return parseIdent();
    case Lexer::kNumber:
    case Lexer::kDot:
    case Lexer::kIntegerPrefix:
        return parseNumber();
    case Lexer::kBracket:
        {
            switch(m_cur->text[0])
            {
            case '[':
                return parseLambda();
            case '(':
                return parseBrackets();
            default:
                auto err = errorFmt(m_cur, "Misplaced bracket: %c", m_cur->text[0]);
                m_cur++;
                return err;
            }
        }
    case Lexer::kOperator:
        {
            if(isUnaryOp(0))
            {
                return parseUnOp();
            }
            else
            {
                auto err = errorFmt(m_cur, "Misplaced binary op: %s", m_cur->text.c_str());
                m_cur++;
                return err;
            }
        }
    default:
        {
            auto err = errorFmt(m_cur, "Unknown token: \'%s\'", m_cur->toString().c_str());
            m_cur++;
            return err;
        }
    }
}

std::unique_ptr<ExpressionAST> Parser::parseNumber()
{
    const Lexer::Token* startToken = m_cur;
    bool hasDot = false;
    bool hasIntegerPrefix = false;
    bool hasExponentialPart = false;
    int numberBase = 10;

    std::string number;
    double mult = 1;

//---------- Dot check --------------//

    if(m_cur->type == Lexer::kDot)
    {
        hasDot = true;
        number += ".";
        m_cur++;
    }

//---------- Integer prefix processing --------------//

    if(!isEnd(m_cur) && m_cur->type == Lexer::kIntegerPrefix)
    {
        if(hasDot)
            return error(m_cur, "Invalid dot before integer prefix");

        hasIntegerPrefix = true;

        if(testToken(0, Lexer::kIntegerPrefix, "0x", false)) // hex
        {
            numberBase = 16;
        }
        else if(testToken(0, Lexer::kIntegerPrefix, "0b", false)) // binary
        {
            numberBase = 2;
        }
        else
            return errorFmt(m_cur, "Unknown integer prefix: %s", m_cur->text.c_str());
        m_cur++;
    }

//---------- Number extraction --------------//

    if(!isEnd(m_cur) && m_cur->type == Lexer::kNumber)
    {
        number += m_cur->text;
        m_cur++;
    }
    else
    {
        return errorFmt(m_cur, "Unknown token while parsing number: %s", m_cur->toString().c_str());
    }

//---------- Dot and floating part extraction --------------//

    if(!isEnd(m_cur) && m_cur->type == Lexer::kDot)
    {
        if(hasDot)
            return errorFmt(m_cur, "Second dot in number");

        if(hasIntegerPrefix)
            return error(m_cur, "Invalid dot in integer value");

        hasDot = true;
        number += ".";
        m_cur++;

        if(!isEnd(m_cur) && m_cur->type == Lexer::kNumber)
        {
            number += m_cur->text;
            m_cur++;
        }
    }

//---------- Exponent extraction --------------//

    if(!isEnd(m_cur) && m_cur->type == Lexer::kNumberExponent)
    {
        if(hasIntegerPrefix)
            return errorFmt(m_cur, "Invalid exponent in integer number format");

        hasExponentialPart = true;
        std::string errorText;
        int64_t exponent = toInteger(std::string(m_cur->text, 1), 10, &errorText);
        if(!errorText.empty())
            return errorFmt(m_cur, "Invalid exponent format: %s (%s)",
                            m_cur->text.c_str(), errorText.c_str());
        mult *= pow(10.0, double(exponent));
        m_cur++;
    }

//---------- Suffix parsing --------------//

    enum
    {
        kForceUnknown,
        kForceFloat,
        kForceInt
    } forceType = kForceUnknown;

    while(!isEnd(m_cur) && m_cur->type == Lexer::kNumberSuffix)
    {
        switch(m_cur->text[0])
        {
        case 'k':
            mult *= 1000;
            break;
        case 'm':
            mult *= 1000 * 1000;
            break;
        case 'g':
            mult *= 1000 * 1000 * 1000;
            break;

        case 'f':
            forceType = kForceFloat;
            break;
        case 'i':
            forceType = kForceInt;
            break;
        default:
            return errorFmt(m_cur, "Unknown number suffix: %s", m_cur->text.c_str());
        }
        m_cur++;
    }

//---------- Number generation --------------//

    if(!hasExponentialPart && (hasIntegerPrefix || !hasDot))
    {
        std::string errorText;
        int64_t value = toInteger(number, numberBase, &errorText) * mult;
        if(!errorText.empty())
            return errorFmt(m_cur, "Invalid integer format: %s (%s)",
                            number.c_str(), errorText.c_str());

        switch(forceType)
        {
        case kForceFloat:
            return std::make_unique<NumberExpressionAST>(startToken - m_begin,
                                                         m_cur - startToken,
                                                         (double)value);
        default:
            return std::make_unique<NumberExpressionAST>(startToken - m_begin,
                                                         m_cur - startToken,
                                                         value);
        }
    }
    else
    {
        std::string errorText;
        double value = toFloat(number, &errorText) * mult;
        if(!errorText.empty())
            return errorFmt(m_cur, "Invalid float format: %s (%s)",
                            number.c_str(), errorText.c_str());

        switch(forceType)
        {
        case kForceInt:
            return std::make_unique<NumberExpressionAST>(startToken - m_begin,
                                                         m_cur - startToken,
                                                         (int64_t)value);
        default:
            return std::make_unique<NumberExpressionAST>(startToken - m_begin,
                                                         m_cur - startToken,
                                                         value);
        }
    }

    return error(m_cur, "Something went wrong while number processing");
}

std::unique_ptr<ExpressionAST> Parser::parseBrackets()
{
    m_cur++; // eat '('
    auto expr = parseExpression();
    if (expr->isInvalid())
        return errorExpr(std::move(expr), "Brackets expression is invalid!");

    if(isEnd(m_cur) || m_cur->text != ")")
        return error(m_cur, "Expected \')\'");
    m_cur++; // eat ')'
    return expr;
}

std::unique_ptr<ExpressionAST> Parser::parseIdent()
{
    auto startToken = m_cur;
    std::string identName = m_cur->text;
    m_cur++;

    if(!testToken(0, Lexer::kBracket, "("))
        return std::make_unique<VariableExpressionAST>(startToken - m_begin,
                                                       m_cur - startToken,
                                                       identName);
    m_cur++; // eat '('

    std::vector< std::unique_ptr<ExpressionAST> > arguments;
    if(!testToken(0, Lexer::kBracket, ")"))
    {
        while(!isEnd(m_cur))
        {
            auto arg = parseExpression();
            if(!arg->isInvalid())
                arguments.push_back(std::move(arg));
            else
                return errorExprFmt(std::move(arg), "Call arguments is invalid (%s)", identName.c_str());

            if(isEnd(m_cur) || testToken(0, Lexer::kBracket, ")"))
                break;

            if(!testToken(0, Lexer::kComma))
                return error(m_cur, "Expected \')\' or \',\' in argument list");
            m_cur++;
        }
    }

    m_cur++; // eat ')'
    return std::make_unique<CallExpressionAST>(startToken - m_begin,
                                               m_cur - startToken,
                                               identName, std::move(arguments));
}

std::unique_ptr<ExpressionAST> Parser::parseBinOpRHS(int expressionPrecedence, std::unique_ptr<ExpressionAST> LHS)
{
    auto startToken = m_cur;
    while (!isEnd(m_cur))
    {
        int tokenPrecedence = getCurrentTokenPrecedence();
        if (tokenPrecedence < expressionPrecedence)
              return LHS;

        auto binOp = m_cur;
        if(!testToken(0, Lexer::kOperator))
        {
            return errorFmt(m_cur, "Not a binary operation: %s", m_cur->toString());
        }
        m_cur++;

        auto RHS = parsePrimary();
        if (RHS->isInvalid())
              return errorExpr(std::move(RHS), "Binary operation: primary expression not found!");

        int nextTokenPrecedence = getCurrentTokenPrecedence();
        if(tokenPrecedence < nextTokenPrecedence)
        {
            RHS = parseBinOpRHS(tokenPrecedence + 1, std::move(RHS));
            if(RHS->isInvalid())
                return errorExpr(std::move(RHS), "Binary operation: failed");
        }

        LHS = std::make_unique<BinaryExpressionAST>(m_cur - m_begin,
                                                    m_cur - startToken,
                                                    binOp->text,
                                                    std::move(LHS), std::move(RHS));
    }

    return error(m_end, "Unexpected end of file");
}

std::unique_ptr<ExpressionAST> Parser::parseUnOp()
{
    auto startToken = m_cur;
    auto unOp = m_cur;
    if(!isUnaryOp(0))
    {
        return errorFmt(m_cur, "Not a unary operation: %s", m_cur->toString());
    }
    m_cur++;

    auto RHS = parsePrimary();
    if (RHS->isInvalid())
          return errorExpr(std::move(RHS), "Unary operation: primary expression not found!");

    return std::make_unique<UnaryExpressionAST>(m_cur - m_begin,
                                                 m_cur - startToken,
                                                 unOp->text,
                                                 std::move(RHS));
}

std::unique_ptr<ExpressionAST> Parser::parseLambda()
{
    auto startToken = m_cur;

    if(!testToken(0, Lexer::kBracket, "["))
        return error(m_cur, "This is not a lambda");
    m_cur++; // eat '['

    std::vector< std::string > arguments;
    if(!testToken(0, Lexer::kBracket, "]"))
    {
        while(!isEnd(m_cur))
        {
            if(testToken(0, Lexer::kIdent))
                arguments.push_back(m_cur->text);
            else
                return errorFmt(m_cur, "Identifyer expected", m_cur->text.c_str());
            m_cur++;

            if(isEnd(m_cur) || testToken(0, Lexer::kBracket, "]"))
                break;

            if(!testToken(0, Lexer::kComma))
                return error(m_cur, "Expected \']\' or \',\' in argument list");
            m_cur++;
        }
    }

    m_cur++; // eat ']'

    auto body = parseExpression();
    if(body->isInvalid())
        return errorExpr(std::move(body), "Lambda body is invalid");


    return std::make_unique<LambdaExpressionAST>(m_cur - m_begin,
                                                 m_cur - startToken,
                                                 arguments,
                                                 std::move(body));
}

std::unique_ptr<ExpressionAST> Parser::parseExpression()
{
    auto LHS = parsePrimary();
    if (LHS->isInvalid())
        return errorExpr(std::move(LHS), "No expression found");
    return parseBinOpRHS(0, std::move(LHS));
}

std::unique_ptr<ExpressionAST> Parser::parseTopLevelExpression()
{
    auto expr = parseExpression();
    if(!expr->isInvalid())
    {
        return expr;
//        return std::make_unique<LambdaExpressionAST>(0, m_end - m_begin,
//                                                      std::vector<std::string>(),
//                                                      std::move(expr));
    }
    return errorExpr(std::move(expr), "Top level expression parse failed!");
}

std::unique_ptr<ExpressionAST> Parser::errorExpr(std::unique_ptr<ExpressionAST> expr, std::string str)
{
//    qCritical() << str.c_str() << "at location" << expr->index() << "(" << expr->toString().c_str() << ")";
    return std::make_unique<ErrorExpressionAST>(expr->index(), expr->size(), str, std::move(expr));
}

std::unique_ptr<ExpressionAST> Parser::error(const Lexer::Token* token, std::string str)
{
//    qCritical() << str.c_str() << "at location" << token - m_begin;
    return std::make_unique<ErrorExpressionAST>(token - m_begin, 1, str);
}

int64_t Parser::toInteger(const std::string& str, int base, std::string* whatError)
{
    int64_t value = 0;
    try
    {
        value = std::stoll(str, 0, base);
    }
    catch(const std::invalid_argument& ex)
    {
        *whatError = ex.what();
        return 0;
    }
    catch(const std::out_of_range& ex)
    {
        *whatError = ex.what();
        return 0;
    }

    whatError->clear();
    return value;
}

double Parser::toFloat(const std::string& str, std::string* whatError)
{
    double value = 0;
    try
    {
        value = std::stod(str, 0);
    }
    catch(const std::invalid_argument& ex)
    {
        *whatError = ex.what();
        return 0;
    }
    catch(const std::out_of_range& ex)
    {
        *whatError = ex.what();
        return 0;
    }

    whatError->clear();
    return value;
}

int Parser::getCurrentTokenPrecedence() const
{
    if(!testToken(0, Lexer::kOperator))
        return -1;
    auto it = m_operatorPrecedence.find(m_cur->text);
    if(it == m_operatorPrecedence.end())
        return 0;
    return it->second;
}

//----------------------------------------------------------



std::string Parser::DebugPrint(const std::unique_ptr<ExpressionAST> &expr)
{
    std::vector<std::string> strings = DebugPrintReq(expr, 0);
    std::string str;
    for(int i = 0; i < strings.size(); i++)
    {
        str += strings[i] + "\n";
    }
    return str;
}

std::vector<std::string> Parser::DebugPrintReq(const std::unique_ptr<ExpressionAST>& expr, int level)
{
    std::vector<std::string> strings;
    if(!expr)
    {
//        strings.push_back("null");
        return strings;
    }

    std::string str = expr->toString();
    strings.push_back(str);

    auto childs = expr->childs();
    for(int i = 0; i < childs.size(); i++)
    {
        bool lastChild = (i == childs.size() - 1);
        auto childStrings = DebugPrintReq(*childs[i], level + 1);
        for(int j = 0; j < childStrings.size(); j++)
        {
            if(j == 0)
                strings.push_back((lastChild ? "|--" : "|--") + childStrings[j]);
            else if(!lastChild)
                strings.push_back("|  " + childStrings[j]);
            else
                strings.push_back("   " + childStrings[j]);
        }
    }
//    if(!childs.empty())
//        if(strings.empty() || !strings.back().empty())
//            strings.push_back("");
    return strings;
}
