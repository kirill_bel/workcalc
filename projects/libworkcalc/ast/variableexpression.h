#ifndef VARIABLEEXPRESSION_H
#define VARIABLEEXPRESSION_H

#include "ast/ast.h"

class VariableExpressionAST : public ExpressionAST
{
    std::string m_name;
public:
    VariableExpressionAST(size_t beginIndex, size_t size, const std::string& name) :
        ExpressionAST(beginIndex, size),
        m_name(name) {}

    std::string name() const;

    bool isInvalid() const override;
    std::string toString() const override;
    llvm::Value* codegen(VMContext* ctx) override;
};

#endif // VARIABLEEXPRESSION_H
