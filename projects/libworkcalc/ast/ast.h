#ifndef AST_H
#define AST_H

#include <cstdint>
#include <string>
#include <memory>
#include <vector>
#include <map>

#include "context.h"
#include "common.h"

namespace llvm {
    class Value;
}
class ExpressionAST
{
    size_t m_beginIndex;
    size_t m_size;
public:
    enum ValueType
    {
        kNone,
        kFloat,
        kInteger
    };
public:
    ExpressionAST(size_t beginIndex, size_t size) :
        m_beginIndex(beginIndex), m_size(size) {}
    virtual ~ExpressionAST() = default;

    virtual std::string toString() const = 0;
    virtual bool isInvalid() const = 0;
    virtual std::vector< const std::unique_ptr<ExpressionAST>* > childs() const;
    virtual llvm::Value* codegen(VMContext* ctx) = 0;

    size_t index() const {return m_beginIndex;}
    size_t size() const {return m_size;}
};

#endif // AST_H
