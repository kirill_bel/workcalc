#ifndef BINARYEXPRESSION_H
#define BINARYEXPRESSION_H

#include "ast/ast.h"

class BinaryExpressionAST : public ExpressionAST
{
    std::string m_operation;
    std::unique_ptr<ExpressionAST> m_lhs;
    std::unique_ptr<ExpressionAST> m_rhs;    
public:
    BinaryExpressionAST(size_t beginIndex, size_t size,
                        const std::string& operation,
                        std::unique_ptr<ExpressionAST> LHS,
                        std::unique_ptr<ExpressionAST> RHS) :
        ExpressionAST(beginIndex, size),
        m_operation(operation),
        m_lhs(std::move(LHS)), m_rhs(std::move(RHS)) {}

    bool isInvalid() const override;
    std::string toString() const override;
    std::vector< const std::unique_ptr<ExpressionAST>* > childs() const override;
    llvm::Value* codegen(VMContext* ctx) override;
protected:
    ValueType prepareValues(VMContext* ctx, llvm::Value*& v1, llvm::Value*& v2, ValueType forceType);
};

#endif // BINARYEXPRESSION_H
