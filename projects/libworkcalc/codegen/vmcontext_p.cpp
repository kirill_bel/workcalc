#include "vmcontext_p.h"

#include <llvm/Analysis/Passes.h>
#include <llvm/ExecutionEngine/ExecutionEngine.h>
#include <llvm/IR/DataLayout.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Value.h>
//#include <llvm/PassManager.h>
#include <llvm/Transforms/Utils/Cloning.h>
#include <llvm/Transforms/Scalar.h>

#include <utility>

llvm::LLVMContext VMContext::globalContext;

VMContext::VMContext() :
    VMContext(std::move(
                  std::make_unique<llvm::Module>(
                      "workcalc jit", VMContext::globalContext)))
{

}

VMContext::VMContext(const VMContext &other) :
    VMContext(std::move(llvm::CloneModule(other.module.get())))
{

}

VMContext::VMContext(std::unique_ptr<llvm::Module> module) :
    module(std::move(module)),
    builder(std::unique_ptr<llvm::IRBuilder<>>(new llvm::IRBuilder<>(module->getContext())))
{
    createEnvironment();


}

VMContext::VMContext(VMContext &&other)
{
    module = std::move(other.module);
    builder = std::move(other.builder);
    other.module = nullptr;
    other.builder = nullptr;
}

VMContext::~VMContext()
{
    builder.reset();
    module.reset();
}

llvm::LLVMContext &VMContext::llvm_context()
{
    return module->getContext();
}

llvm::Value *VMContext::namedValue(const std::string &name)
{
    return namedValues[name];
}

void VMContext::clearNamedValues()
{
    namedValues.clear();
}

void VMContext::setNamedValue(const std::string &name, llvm::Value *value)
{
    namedValues[name] = value;
}

llvm::GlobalVariable *VMContext::createGlobalVariable(std::string name, llvm::Type *type)
{
    module->getOrInsertGlobal(name, type);
    llvm::GlobalVariable* var = module->getNamedGlobal(name);
    var->setLinkage(llvm::GlobalValue::CommonLinkage);
    return var;
}

llvm::AllocaInst *VMContext::createEntryBlockAlloca(llvm::Function *func,
                                                    const std::string &name,
                                                    llvm::Type* type)
{
    llvm::IRBuilder<> tmp_builder(&func->getEntryBlock(),
                                    func->getEntryBlock().begin());
    return tmp_builder.CreateAlloca(type, 0, name.c_str());
}

std::string VMContext::print() const
{
    std::string str;
    llvm::raw_string_ostream os(str);
    module->print(os, nullptr);
    return str;
}

void VMContext::createEnvironment()
{
    std::vector<llvm::Type*> members_types;
    members_types.push_back(llvm::Type::getInt64Ty(llvm_context())); // Num arguments
    members_types.push_back(llvm::Type::getInt64PtrTy(llvm_context())); // Types of arguments
    members_types.push_back(llvm::Type::getInt8PtrTy(llvm_context())); // Pointer to array of elements
    members_types.push_back(llvm::Type::getInt64Ty(llvm_context())); // Return value type
    members_types.push_back(llvm::Type::getVoidTy(llvm_context())); // Pointer to return value

    m_callbackStructType =
            llvm::StructType::create(llvm_context(), members_types, "__callbackStruct", false);



    llvm::FunctionType* callbackFuncType = llvm::FunctionType::get(
                    llvm::Type::getDoubleTy(ctx->llvm_context()),
                    types,
                    false);
}

llvm::Value *VMContext::createExternalCall(std::string functionName, std::vector<llvm::Type *> arguments)
{
    std::vector<llvm::Type*> argument_types;
    members_types.push_back(llvm::Type::getInt64Ty(llvm_context())); // Num arguments
    members_types.push_back(llvm::Type::getInt64PtrTy(llvm_context())); // Types of arguments
    members_types.push_back(llvm::Type::getInt8PtrTy(llvm_context())); // Pointer to array of elements
    members_types.push_back(llvm::Type::getInt64Ty(llvm_context())); // Return value type
    members_types.push_back(llvm::Type::getVoidTy(llvm_context())); // Pointer to return value

    llvm::FunctionType* callbackFuncType = llvm::FunctionType::get(
                    llvm::Type::getDoubleTy(ctx->llvm_context()),
                    types,
                    false);
}
