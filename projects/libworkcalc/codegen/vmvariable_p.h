#ifndef VMVARIABLE_H
#define VMVARIABLE_H

#include "vmmodule_p.h"

class VmVariable
{
public:
    static std::shared_ptr<VmVariable>
    Create(std::string name, llvm::Type *type);

    VmVariable(std::shared_ptr<VmModule> module,
               llvm::GlobalVariable* variable);

private:
    std::shared_ptr<VmModule> m_module;
    llvm::GlobalVariable* m_variable = nullptr;
};

class VmVariable_Int64
{
public:
private:
    llvm::GlobalVariable* m_variable = nullptr;
};

#endif // VMVARIABLE_H
