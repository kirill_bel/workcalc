#ifndef LEXER_H
#define LEXER_H

#include <string>
#include <vector>

class Lexer
{
public:
    enum TokenType
    {
        kError = -2,
        kUnknown = -1,
        kIdent,
        kNumber,
        kIntegerPrefix,
        kNumberExponent,
        kNumberSuffix,
        kDot,
        kSemicolon,
        kComma,
        kBracket,
        kOperator,
        kKeyword,
        kEOF
    };

    struct Token
    {
        Token();
        Token(TokenType type, size_t index, size_t size,
              std::string text = "") :
            type(type), index(index), size(size), text(text)
        {}

        TokenType type;
        size_t index;
        size_t size;

        std::string text;      
        std::string toString() const;
    };

public:
    Lexer();

    int tokenize(const std::string& str, std::vector<Lexer::Token>* outTokens);

protected:
    bool isEnd(const char* ptr);
private:
    int parseSingleSymbol();
    int parseIdentifier();
    int parseNumber();
    int parseNumberPrefix();
    int parseNumberExponent();
    int parseNumberSuffix();
private:
    const char* m_begin = nullptr;
    const char* m_cur = nullptr;
    const char* m_end = nullptr;
    std::vector<Lexer::Token> *m_tokens = nullptr;
};

#endif // LEXER_H
