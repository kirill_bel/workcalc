#include "workcalc.h"

#include "codegen/vmcontext_p.h"

WorkCalcProgramm::WorkCalcProgramm()
{

}

WorkCalcProgramm::WorkCalcProgramm(std::shared_ptr<WorkCalcContext> context) :
    m_context(context)
{
}

WorkCalcProgramm::~WorkCalcProgramm()
{

}

int WorkCalcProgramm::eval(const std::string &code)
{
    if(setSource(code) < 0)
        return -1;

    if(tokenize() < 0)
        return -1;

    if(parse() < 0)
        return -1;

    if(codegen() < 0)
        return -1;

    return 0;
}

int WorkCalcProgramm::setSource(const std::string &code)
{
    m_source = code;
    m_stage = m_source.empty() ? kEmpty : kSourceOk;
    return (m_stage == kSourceOk) ? 0 : -1;
}

std::string WorkCalcProgramm::source() const
{
    return m_source;
}

int WorkCalcProgramm::tokenize()
{
    if(m_stage < kSourceOk)
        return -1;

    Lexer lex;
    m_tokens.clear();
    if(lex.tokenize(m_source, &m_tokens) < 0)
        return -1;

    m_stage = kTokensOk;
    return 0;
}

const std::vector<Lexer::Token> &WorkCalcProgramm::tokens() const
{
    return m_tokens;
}

int WorkCalcProgramm::parse()
{
    if(m_stage < kTokensOk)
        return -1;

    Parser parser;
    m_ast.clear();
    if(parser.parse(m_tokens, &m_ast) < 0)
        return -1;

    m_stage = kAstOk;
    return 0;
}

const std::vector<std::unique_ptr<ExpressionAST> > &WorkCalcProgramm::ast() const
{
    return m_ast;
}

int WorkCalcProgramm::codegen()
{
    if(!m_context)
        return -1;

    if(m_stage < kAstOk)
        return -1;

    for(int i = 0; i < m_ast.size(); i++)
    {
        if(m_ast[i]->isInvalid())
            continue;

        if(llvm::Function* func =
                static_cast<llvm::Function*>(m_ast[i]->codegen(m_context->context())))
        {

        }
    }

    m_compiledSource = m_context->context()->print();
    m_stage = kAstOk;
    return 0;
}

std::shared_ptr<WorkCalcContext> WorkCalcProgramm::context() const
{
    return m_context;
}

void WorkCalcProgramm::setContext(std::shared_ptr<WorkCalcContext> ctx)
{
    m_context = ctx;
}

std::string WorkCalcProgramm::compiledSource() const
{
    return m_compiledSource;
}
