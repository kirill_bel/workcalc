#ifndef VM_H
#define VM_H

#include <memory>
#include <string>
#include <functional>

//class WorkCalcState
//{
//public:
//    size_t numArguments() const;
//    bool isInteger() const;
//    int64_t
//};

class VMContext;
class WorkCalcContext
{
public:

public:
    WorkCalcContext();
    ~WorkCalcContext();
    VMContext* context();

//    int setInt(const std::string& name, int64_t value);
//    int setDouble(const std::string& name, double value);
//    int setFunction(const std::string& name, std::function)


private:
    std::unique_ptr<VMContext> m_context;
};

#endif // VM_H
