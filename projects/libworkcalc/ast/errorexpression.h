#ifndef ERROREXPRESSION_H
#define ERROREXPRESSION_H

#include "ast/ast.h"

class ErrorExpressionAST : public ExpressionAST
{
    std::string m_error;
    std::unique_ptr<ExpressionAST> m_child;
public:
    ErrorExpressionAST(size_t beginIndex, size_t size,
                       const std::string& errorText,
                       std::unique_ptr<ExpressionAST> child = nullptr) :
        ExpressionAST(beginIndex, size),
        m_error(errorText),
        m_child(std::move(child)) {}

    bool isInvalid() const override;
    std::string toString() const override;
    std::vector< const std::unique_ptr<ExpressionAST>* > childs() const override;
    llvm::Value* codegen(VMContext* ctx) override;
};

#endif // ERROREXPRESSION_H
