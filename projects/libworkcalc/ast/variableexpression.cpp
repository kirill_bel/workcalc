#include "variableexpression.h"

#include "codegen/vmcontext_p.h"

std::string VariableExpressionAST::name() const
{
    return m_name;
}

bool VariableExpressionAST::isInvalid() const
{
    return m_name.empty();
}

std::string VariableExpressionAST::toString() const
{
    return utils::format("variable \'%s\'", m_name.c_str());
}

llvm::Value *VariableExpressionAST::codegen(VMContext *ctx)
{
    llvm::Value *v = ctx->namedValue(m_name);
    if(!v)
    {
        utils::error("Unknown variable name: \"%s\"", m_name.c_str());
        return nullptr;
    }
    return v;
}
