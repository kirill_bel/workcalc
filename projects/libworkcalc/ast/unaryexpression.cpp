#include "unaryexpression.h"

#include "codegen/vmcontext_p.h"

bool UnaryExpressionAST::isInvalid() const
{
    return m_operation.empty() ||
            !m_rhs || m_rhs->isInvalid();
}

std::string UnaryExpressionAST::toString() const
{
    return utils::format("unop \'%s\'", m_operation.c_str());
}

std::vector<const std::unique_ptr<ExpressionAST> *> UnaryExpressionAST::childs() const
{
    std::vector<const std::unique_ptr<ExpressionAST> *> vec;
    vec.push_back(&m_rhs);
    return vec;
}

llvm::Value *UnaryExpressionAST::codegen(VMContext* ctx)
{
    llvm::Value* operand_value = m_rhs->codegen(ctx);
    if(!operand_value)
    {
        return nullptr;
    }

    return nullptr;
}
