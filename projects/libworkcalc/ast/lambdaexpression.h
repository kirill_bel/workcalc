#ifndef LAMBDAEXPRESSION_H
#define LAMBDAEXPRESSION_H

#include "ast/ast.h"

class LambdaExpressionAST : public ExpressionAST
{
    std::vector< std::string > m_arguments;
    std::unique_ptr<ExpressionAST> m_body;
    std::string m_name;
public:
    LambdaExpressionAST(size_t beginIndex, size_t size,
                        const std::vector< std::string >& arguments,
                        std::unique_ptr<ExpressionAST> body,
                        std::string name = "") :
        ExpressionAST(beginIndex, size),
        m_arguments(arguments),
        m_body(std::move(body)),
        m_name(name) {}

    bool isInvalid() const override;
    std::string toString() const override;
    std::vector< const std::unique_ptr<ExpressionAST>* > childs() const override;
    llvm::Value* codegen(VMContext* ctx) override;
};

#endif // LAMBDAEXPRESSION_H
