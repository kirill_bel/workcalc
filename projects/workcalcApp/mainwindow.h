#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <workcalc.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_evalBtn_clicked();

private:
    Ui::MainWindow *ui;
    std::shared_ptr<WorkCalcContext> m_conext;
};

#endif // MAINWINDOW_H
