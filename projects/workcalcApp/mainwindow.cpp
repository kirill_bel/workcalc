#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->splitter->setStretchFactor(0, 2);
    ui->splitter->setStretchFactor(1, 3);
    ui->splitter->setStretchFactor(2, 4);

    m_conext = std::make_shared<WorkCalcContext>();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_evalBtn_clicked()
{
    ui->lexerOutput->clear();
    ui->parserOutput->clear();
    ui->compilerOutput->clear();

    WorkCalcProgramm prg(m_conext);

    prg.eval(ui->expressionEdit->text().toStdString());

    for(int i = 0; i < prg.tokens().size(); i++)
    {
        ui->lexerOutput->append(prg.tokens()[i].toString().c_str());
    }

    for(int i = 0; i < prg.ast().size(); i++)
    {
        ui->parserOutput->append(Parser::DebugPrint(prg.ast()[i]).c_str());
        ui->parserOutput->append("################################");
    }

    ui->compilerOutput->append(prg.compiledSource().c_str());
}
