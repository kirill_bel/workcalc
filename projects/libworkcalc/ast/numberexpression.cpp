#include "numberexpression.h"

#include "codegen/vmcontext_p.h"

bool NumberExpressionAST::isInvalid() const
{
    return false;
}

std::string NumberExpressionAST::toString() const
{
    switch(m_type)
    {
    case kInteger:
        return utils::format("integer \'%lld\'", m_value.valueInt);
    case kFloat:
        return utils::format("float \'%lf\'", m_value.valueFloat);
    }
    return "unknown number";
}

llvm::Value *NumberExpressionAST::codegen(VMContext* ctx)
{
    switch(m_type)
    {
    case kFloat:
        return llvm::ConstantFP::get(ctx->llvm_context(), llvm::APFloat(m_value.valueFloat));
    case kInteger:
        return llvm::ConstantInt::get(llvm::IntegerType::get(ctx->llvm_context(), 64),
                                      m_value.valueInt, false);
    default:
        utils::error("Invalid number type");
        return nullptr;
    }
}
