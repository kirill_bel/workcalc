#ifndef WORKCALC_H
#define WORKCALC_H

#include <memory>
#include <string>

#include "context.h"
#include "parsing/lexer.h"
#include "parsing/parser.h"

class WorkCalcProgramm
{
public:
    enum Stage
    {
        kEmpty = 0,
        kSourceOk,
        kTokensOk,
        kAstOk,
        kCodegenOk
    };

public:
    WorkCalcProgramm();
    WorkCalcProgramm(std::shared_ptr<WorkCalcContext> context);
    ~WorkCalcProgramm();

    int eval(const std::string& code);

    int setSource(const std::string& code);
    std::string source() const;

    int tokenize();
    const std::vector<Lexer::Token>& tokens() const;

    int parse();
    const std::vector<std::unique_ptr<ExpressionAST>>& ast() const;

    int codegen();

    std::shared_ptr<WorkCalcContext> context() const;
    void setContext(std::shared_ptr<WorkCalcContext> ctx);

    std::string compiledSource() const;
private:
    std::string m_source;
    std::string m_compiledSource;
    std::shared_ptr<WorkCalcContext> m_context;
    std::vector<Lexer::Token> m_tokens;
    std::vector< std::unique_ptr<ExpressionAST> > m_ast;
    Stage m_stage = kEmpty;
};

#endif // WORKCALC_H
