#include "common.h"

namespace utils {
    std::string format_va(const char* fmt, va_list args)
    {
    //        va_start(args, fmt);
        char buf[32];
        size_t n = std::vsnprintf(buf, sizeof(buf), fmt, args);
    //        va_end(args);

        // Static buffer large enough?
        if (n < sizeof(buf)) {
            return {buf, n};
        }

        // Static buffer too small
        std::string s;//(n+1, ' ');
    //        va_start(args, fmt);
        char* alloc_buf = new char[n+1];
        std::vsnprintf(alloc_buf, n+1, fmt, args);
        s = alloc_buf;
        delete [] alloc_buf;
    //        va_end(args);

        return s;
    }
}
