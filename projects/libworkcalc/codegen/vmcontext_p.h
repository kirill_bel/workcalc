#ifndef VMCONTEXT_P_H
#define VMCONTEXT_P_H

#include <llvm/ExecutionEngine/ExecutionEngine.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>

#include <map>
#include <string>
#include <functional>

class VMContext
{
    std::map<std::string, llvm::Value*> namedValues;

    VMContext(const VMContext &other);
    VMContext(std::unique_ptr<llvm::Module> module);
    VMContext(VMContext &&other);

    static llvm::LLVMContext globalContext;
    llvm::Type* m_callbackStructType = nullptr;
    llvm::Function* m_callbackFunc = nullptr;
public:
    typedef std::function<int64_t(int)> ExternalFunctionType;
public:
    VMContext();
    ~VMContext();

    std::unique_ptr<llvm::IRBuilder<>> builder;
    std::unique_ptr<llvm::Module> module;

    llvm::LLVMContext &llvm_context();

    llvm::Value *namedValue(const std::string &name);
    void clearNamedValues();
    void setNamedValue(const std::string &name, llvm::Value *value);

    llvm::GlobalVariable* createGlobalVariable(std::string name, llvm::Type* type);
    llvm::AllocaInst* createEntryBlockAlloca(llvm::Function* func,
                                             const std::string& name,
                                             llvm::Type* type);

    void setExternalFunction(std::string name, )
    llvm::Value* createExternalCall(std::string functionName, std::vector<llvm::Type*> arguments);

    std::string print() const;

    void createEnvironment();
};

#endif // VMCONTEXT_P_H
