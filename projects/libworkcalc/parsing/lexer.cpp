#include "lexer.h"

#include <cctype>

Lexer::Lexer()
{

}

std::string Lexer::Token::toString() const
{
    switch(type)
    {
    case kError:            return "Error: " + this->text;
    case kUnknown:          return "Unknown: " + this->text;
    case kIdent:            return "Ident: " + this->text;
    case kNumber:           return "Number: " + this->text;
    case kIntegerPrefix:    return "IntegerPrefix: " + this->text;
    case kNumberExponent:   return "NumberExponent: " + this->text;
    case kNumberSuffix:     return "NumberSuffix: " + this->text;
    case kDot:              return "Dot: " + this->text;
    case kSemicolon:        return "Semicolon";
    case kComma:            return "Comma: " + this->text;
    case kBracket:          return "Bracket: " + this->text;
    case kOperator:         return "Operator: " + this->text;
    case kKeyword:          return "Keyword: " + this->text;
    case kEOF:              return "EOF";
    }
    return "UnknownType";
}

int Lexer::tokenize(const std::string &str, std::vector<Lexer::Token> *outTokens)
{
    m_begin = str.c_str();
    m_cur = m_begin;
    m_end = m_begin + str.size();
    m_tokens = outTokens;

    int ret;
    while(!isEnd(m_cur))
    {
        while(!isEnd(m_cur) && std::isspace(*m_cur))
            m_cur++;

        if(isEnd(m_cur))
            break;

        ret = parseIdentifier();
        if(ret > 0)
            continue;

        ret = parseNumber();
        if(ret > 0)
            continue;

        ret = parseSingleSymbol();
        if(ret > 0)
            continue;

        outTokens->push_back(Token(kUnknown, m_cur - m_begin, 1,
                                   std::string(m_cur, 1)));
        m_cur++;
    }

    outTokens->push_back(Token(kEOF, m_cur - m_begin, 1,
                               ""));
    return 0;
}

bool Lexer::isEnd(const char *ptr)
{
    return ptr >= m_end;
}

int Lexer::parseSingleSymbol()
{
    if(!isEnd(m_cur + 1))
    {
        std::string str(m_cur, 2);
        if(str == "->" ||
            str == "<-" ||
            str == ">>" ||
            str == "<<")
        {
            m_tokens->push_back(Token(kOperator, m_cur - m_begin, 2, std::string(m_cur, 2)));
            m_cur += 2;
            return 2;
        }
    }

    switch(*m_cur)
    {    
    case '(':
    case ')':
    case '[':
    case ']':
    case '{':
    case '}':
        m_tokens->push_back(
                    Token(kBracket, m_cur - m_begin, 1, std::string(m_cur, 1)));
        m_cur++;
        return 1;
    case '*':
    case '/':
    case '+':
    case '-':
    case '^':
    case '>':
    case '<':
    case '=':
    case '&':
    case '|':
    case '!':
    case '~':
        m_tokens->push_back(Token(kOperator, m_cur - m_begin, 1, std::string(m_cur, 1)));
        m_cur++;
        return 1;
    case '.':
        m_tokens->push_back(Token(kDot, m_cur - m_begin, 1, std::string(m_cur, 1)));
        m_cur++;
        return 1;
    case ';':
        m_tokens->push_back(Token(kSemicolon, m_cur - m_begin, 1, std::string(m_cur, 1)));
        m_cur++;
        return 1;
    case ',':
        m_tokens->push_back(Token(kComma, m_cur - m_begin, 1, std::string(m_cur, 1)));
        m_cur++;
        return 1;
    }

    return 0;
}

int Lexer::parseIdentifier()
{
    const char* str = m_cur;
    if(std::isalpha(*str))
    {
        std::string data(str, 1);
        str++;

        while(std::isalnum(*str))
        {
            data += *str;
            str++;
        }

        m_tokens->push_back(Token(kIdent, m_cur - m_begin, str - m_cur, data));
        m_cur = str;
        return data.size();
    }
    return 0;
}

bool isHex(char c)
{
    return std::isdigit(c) ||
            std::tolower(c) == 'a' ||
            std::tolower(c) == 'b' ||
            std::tolower(c) == 'c' ||
            std::tolower(c) == 'd' ||
            std::tolower(c) == 'e' ||
            std::tolower(c) == 'f';
}

bool isBinary(char c)
{
    return (c == '0') || (c == '1');
}
// [0-9].[0-9][e+-(0-9)][k,m,g,..][i,b,f]
// 0x000
int Lexer::parseNumber()
{
    const char* start = m_cur;

    int numberBase = parseNumberPrefix();
    if(numberBase < 0)
        return -1;
    else if(numberBase == 0)
        numberBase = 10;

    std::string number;
    const char* str = m_cur;


    switch(numberBase)
    {
    case 2:
        while(!isEnd(str) && isBinary(*str))
        {
            number += *str;
            str++;
        }
        break;
    case 10:
        while(!isEnd(str) && std::isdigit(*str))
        {
            number += *str;
            str++;
        }
        break;
    case 16:
        while(!isEnd(str) && isHex(*str))
        {
            number += *str;
            str++;
        }
        break;
    default:
        return -1;
    }

    if(!number.empty())
    {
        m_tokens->push_back(Token(kNumber, str - m_begin, str - m_cur, number));
        m_cur = str;
    }

    if(parseNumberExponent() == -1)
        return -1;

    if(parseNumberSuffix() == -1)
        return -1;

    // Check for number corruption
    const char* corrupt_start = m_cur;
    std::string corruption;
    while(!isEnd(m_cur) && std::isalnum(*m_cur))
    {
        corruption += *m_cur;
        m_cur++;
    }

    if(!corruption.empty())
    {
        m_tokens->push_back(Token(kUnknown, m_cur - m_begin, m_cur - corrupt_start, corruption));
        m_cur = str;
    }

    return m_cur - start;
}

int Lexer::parseNumberPrefix()
{
    int base = 0;
    if(!isEnd(m_cur) && (*m_cur == '0') && !isEnd(m_cur+1) && std::isalpha(m_cur[1]))
    {
        switch(std::tolower(m_cur[1]))
        {
        case 'x':
            base = 16;
            m_tokens->push_back(Token(kIntegerPrefix, m_cur - m_begin, 2, std::string(m_cur, 2)));
            m_cur += 2;
            break;
        case 'b':
            base = 2;
            m_tokens->push_back(Token(kIntegerPrefix, m_cur - m_begin, 2, std::string(m_cur, 2)));
            m_cur += 2;
            break;
        case 'e':
            return 0;
        default:
            return -1;
        }
    }

    return base;
}

int Lexer::parseNumberExponent()
{
    const char* str = m_cur;
    if(!isEnd(str) && (std::tolower(*str) == 'e') &&
            !isEnd(str+1) && ((str[1] == '+') || (str[1] == '-')))
    {
        str += 2;

        const char* numStart = str;
        while(!isEnd(str) && std::isdigit(*str))
        {
            str++;
        }

        if((str - numStart) == 0)
            return -1;

        size_t size = str - m_cur;
        m_tokens->push_back(Token(kNumberExponent, m_cur - m_begin, size,
                                  std::string(m_cur, size)));
        m_cur = str;
        return size;
    }
    return 0;
}

int Lexer::parseNumberSuffix()
{
    const char* start = m_cur;

    while(!isEnd(m_cur) && std::isalpha(*m_cur))
    {
        m_tokens->push_back(Token(kNumberSuffix, m_cur - m_begin, 1, std::string(m_cur, 1)));
        m_cur++;
    }

    return m_cur - start;
}
